import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(17, GPIO.OUT)        # PWR_KEY is GPIO17


GPIO.output(17, False)
time.sleep(2)
GPIO.output(17, True)


